import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';


import Pages from './Pages';
import { initFirebase } from './services/firebase';
import * as serviceWorker from './serviceWorker';

import './index.css';

initFirebase();

// react-redux-firebase config
const rrfConfig = {
  userProfile: 'users',
  // useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
}

const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig),
)(createStore)

// Add firebase to reducers
const rootReducer = combineReducers({
  firebase: firebaseReducer,
})

// Create store with reducers and initial state
const initialState = {}
const store = createStoreWithFirebase(rootReducer, initialState, applyMiddleware(logger))

// Setup react-redux so that connect HOC can be used
const App = () => (
  <Provider store={store}>
    <Pages />
  </Provider>
);

// export const init = ({ element, width, height }) => {
  ReactDOM.render(<App/>, document.getElementById('root'));
  // };
  
  // full page editor config
  // comment init function to create build for widget
  /* init({
    width : `calc(100% - 2px)`,
    height : `${window.screen.availHeight - 74}px`,
    element : document.getElementById('root')
  });
  */
  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: http://bit.ly/CRA-PWA
  serviceWorker.unregister();

