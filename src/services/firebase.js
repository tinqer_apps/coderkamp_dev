import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import firebaseConfig from '../config/firebase.config';

export const firebaseSessionPath = 'live-sessions';

window['firebase'] = firebase;

export const initFirebase = () => {
  firebase.initializeApp(firebaseConfig);
};


export const getFirebaseRef = (key) => {
  if(key){
    return firebase.database().ref(firebaseSessionPath).child(key);
  }
  return firebase.database().ref(firebaseSessionPath).push();
};

export const addUserName = (refKey, userId, userName) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}/users/${userId}`)
    .set({ userName });
};

export const addSession = (refKey, ownerId, userId, userName, enableChat) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}`)
    .set({
      users : {
        [userId] :{
          userName
        }
      },
      session :{
        ownerId : ownerId,
        ownerName : userName,
        enableChat,
        timestamp : Date.now()
      }
    });
}

export const confirmBeforeLeave = (refKey, userId) => {
  window.onbeforeunload = function() {
    return 'Do you want to leave?';
  };
  window.onunload = function() {
    // removeUser(refKey, userId);
  };
};
export const removeUser = (refKey, userId) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}/users/${userId}`)
    .remove();
};
export const updateEditorMode = (refKey, mode) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}/mode`)
    .set(mode);
};

export const updateEditorTheme = (refKey, theme) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}/theme`)
    .set(theme);
};

export const offListening = (refKey, key) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}/${key}`)
    .off();
};

export const getUserData = refKey => {
  const data = localStorage.getItem(refKey);
  const userData = data ? JSON.parse(data)  : {};
  return userData.userName && userData.userId ? userData : null;
};

export const saveUserData = (refKey, userId ,userName, enableChat=false) => {
  localStorage.setItem([refKey], JSON.stringify({ userName, userId, enableChat }));
};

export const sendMessage = (refKey, userName, userId, message) => {
  return firebase
    .database()
    .ref(`${firebaseSessionPath}/${refKey}/messages`)
    .push({
      userName,
      message,
      userId,
      timestamp: Date.now(),
    });
};

export const logout = ()=> {
  firebase.auth().signOut();
}

export const sendPasswordResetEmail = (email) =>{
  const auth = firebase.auth();
  return auth.sendPasswordResetEmail(email);
}

export const firebaseSet = (ref, data) =>{
  return firebase.database().ref(ref).set(data);
}

// export const onDisconnect = (refKey, userId) => {
//   const presenceRef = firebase.database().ref(`${firebaseSessionPath}/${refKey}/users/${userId}/active`);
//   presenceRef.onDisconnect().set(false);
//   console.log('ondisconnect');
// };

// export const onConnect = (refKey, userId, userName) => {
//   const connectedRef = firebase.database().ref('.info/connected');
//   connectedRef.on('value', function(snap) {
//     const ref = firebase
//     .database()
//     .ref(`${firebaseSessionPath}/${refKey}/users/${userId}`)
//     if (snap.val() === true) {
//         ref.update({
//           userName,
//           active : true
//         });
//       console.log('onconnect true');

//     } else {
//       console.log('onconnect false');
//     }
//   });
// };
