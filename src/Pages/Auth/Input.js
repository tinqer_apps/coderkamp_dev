import React, {Fragment} from 'react';

const Input =(props)=>{
    const { type, value, placeholder, error, onChange } = props;
    return (
        <Fragment>
            <input type={type} className="form-control" placeholder={placeholder} value={value} onChange={onChange} />
            <p className="text-error">{(error || '').toUpperCase()}</p>
        </Fragment>
    )
}

export default Input;