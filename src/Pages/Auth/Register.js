import React from 'react';
import { Link } from 'react-router-dom';
import { withFirebase } from 'react-redux-firebase';
import Input from './Input';
import Img from '../../assets/imgs/avatar_2x.png';
import { allInputsAreFilled, validateEmail } from './utils';
import './style.css';

class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      form: {
        displayName: '',
        email: '',
        password: '',
        confirmPassword: ''
      },
      formErrors: {},
      firebaseError: ''
    };
  }

  handleInputChange = (key, event) => {
    const { form } = this.state;
    form[key] = event.target.value;
    this.setState({ form });
  };

  handleSubmit = () => {
    const isFormValid = this.checkValidation();
    if (isFormValid) {
      this.signUpWithEmailPassword();
    }
  };

  signUpWithEmailPassword = () => {
    const { email, password, displayName } = this.state.form;
    this.props.firebase.createUser({ email, password }, { displayName, email, joinedAt: Date.now() })
    .catch(err => {
      this.setState({ firebaseError: err.message });
    });
  };

  checkValidation = () => {
    const { form } = this.state;
    let formErrors = allInputsAreFilled(form);
    if (formErrors) {
      this.setState({ formErrors });
      return;
    }
    formErrors = {};
    // Full Name must have minumum 4 characters
    if (form.displayName.length < 4) {
      formErrors['displayName'] = 'Full Name must have minimum 4 characters.';
    }
    // check for valid email format
    if (!validateEmail(form.email)) {
      formErrors['email'] = 'Invalid email format.';
    }
    // password must have minimum 6 characters
    if (form.password.length < 6) {
      formErrors['password'] = 'Password must have minimum 6 characters.';
    } else if (form.password !== form.confirmPassword) {
    // password and confirm password must be equal
      formErrors['confirmPassword'] = 'Passwords are not matched.';
    }
    this.setState({ formErrors });
    const isValid = Object.keys(formErrors).length === 0;
    return isValid;
  };

  render() {
    const {
      form: { displayName, email, password, confirmPassword },
      formErrors,
      firebaseError
    } = this.state;
    return (
      <div className="custom-body">
        <div className="container">
          <div className="card card-container">
            <img id="profile-img" className="profile-img-card" src={Img} alt="userDemoImage" />
            <p className="text-error">{firebaseError || ''}</p>
            <div className="form-signin mb-1">
              <Input
                type="text"
                className="form-control"
                placeholder="Full Name"
                value={displayName}
                error={formErrors['displayName']}
                onChange={e => this.handleInputChange('displayName', e)}
              />

              <Input
                type="email"
                className="form-control"
                placeholder="Email address"
                value={email}
                error={formErrors['email']}
                onChange={e => this.handleInputChange('email', e)}
              />

              <Input
                type="password"
                className="form-control"
                placeholder="Password"
                value={password}
                error={formErrors['password']}
                onChange={e => this.handleInputChange('password', e)}
              />

              <Input
                type="password"
                className="form-control"
                placeholder="Confirm Password"
                value={confirmPassword}
                error={formErrors['confirmPassword']}
                onChange={e => this.handleInputChange('confirmPassword', e)}
              />
              <button className="btn btn-lg btn-primary btn-block btn-signin" type="submit" onClick={this.handleSubmit}>
                Create Account
              </button>
            </div>
            Already have an account?
            <Link className="forgot-password" to="/login">
              Sign In
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default withFirebase(Register);
