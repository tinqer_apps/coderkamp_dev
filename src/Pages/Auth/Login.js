import React from 'react';
import { Link } from 'react-router-dom';
import { withFirebase } from 'react-redux-firebase';
import Input from './Input';
import Img from '../../assets/imgs/avatar_2x.png';
import { allInputsAreFilled, validateEmail } from './utils';
import './style.css';

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      form: {
        email: '',
        password: ''
      },
      formErrors: {},
      firebaseError : ''
    };
  }

  handleInputChange = (key, event) => {
    const { form } = this.state;
    form[key] = event.target.value;
    this.setState({ form });
  };

  handleSubmit = () => {
    const isFormValid = this.checkValidation();
    if (isFormValid) {
      this.loginInWithEmailPassword();
    }
  };

  loginInWithEmailPassword = () => {
    const { email, password } = this.state.form;
    this.props.firebase
      .login({
        email,
        password
      })
      .catch(err => {
        this.setState({ firebaseError: err.message });
      });
  };
  loginWithGoogle = () =>{
    this.props.firebase
      .login({
          provider: "google",
          type: "popup",
      })
  }
  checkValidation = () => {
    const { form } = this.state;
    let formErrors = allInputsAreFilled(form);
    if (formErrors) {
      this.setState({ formErrors });
      return;
    }
    formErrors = {};
    // check for valid email format
    if (!validateEmail(form.email)) {
      formErrors['email'] = 'Invalid email format.';
    }
    // password must have minimum 6 characters
    if (form.password.length < 6) {
      formErrors['password'] = 'Password must have minimum 6 characters.';
    }

    this.setState({ formErrors });
    const isValid = Object.keys(formErrors).length === 0;
    return isValid;
  };

  render() {
    const {
      form: { email, password },
      formErrors,
      firebaseError
    } = this.state;
    return (
      <div className="custom-body">
        <div className="container">
          <div className="card card-container">
            <img id="profile-img" className="profile-img-card" src={Img} alt="userDemoImage" />
            <p className="text-error">{firebaseError || ''}</p>
            <div className="form-signin mb-1">
              <Input
                type="email"
                className="form-control"
                placeholder="Email address"
                value={email}
                error={formErrors['email']}
                onChange={e => this.handleInputChange('email', e)}
              />

              <Input
                type="password"
                className="form-control"
                placeholder="Password"
                value={password}
                error={formErrors['password']}
                onChange={e => this.handleInputChange('password', e)}
              />

              <button className="btn btn-lg btn-primary btn-block btn-signin" type="submit" onClick={this.handleSubmit}>
                Sign in
              </button>
            </div>
            <Link className="forgot-password" to="/forgotPassword">
              Forgot the password?
            </Link>
            <br/>
              <button className="btn btn-lg btn-block btn-signin btn-danger" onClick={this.loginWithGoogle}>
                Sign in via Google
              </button>
          </div>
        </div>
      </div>
    );
  }
}

export default withFirebase(Login);
