import React from 'react';
import { Link } from 'react-router-dom';
import Input from './Input';
import Img from '../../assets/imgs/avatar_2x.png';
import { allInputsAreFilled, validateEmail } from './utils';
import { sendPasswordResetEmail } from '../../services/firebase';
import './style.css';

class ForgotPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      form: {
        email: '',
      },
      formErrors: {},
      firebaseMessage : null
    };
  }

  handleInputChange = (key, event) => {
    const { form } = this.state;
    form[key] = event.target.value;
    this.setState({ form });
  };

  handleSubmit = () => {
    const isFormValid = this.checkValidation();
    if (isFormValid) {
      const promise=sendPasswordResetEmail(this.state.form.email);
      promise.then((d)=>{
          console.log(d);
        this.setState({ 
            firebaseMessage: {
                type : 'SUCCESS',
                text : 'A reset password link is sent to your mail.'
            } 
        });
      })
      .catch(err=>{
        this.setState({ 
            firebaseMessage: {
                type : 'ERROR',
                text : err.message
            } 
        });

      })
    }
  };

  checkValidation = () => {
    const { form } = this.state;
    let formErrors = allInputsAreFilled(form);
    if (formErrors) {
      this.setState({ formErrors });
      return;
    }
    formErrors = {};
    // check for valid email format
    if (!validateEmail(form.email)) {
      formErrors['email'] = 'Invalid email format.';
    }

    this.setState({ formErrors });
    const isValid = Object.keys(formErrors).length === 0;
    return isValid;
  };

  render() {
    const {
      form: { email },
      formErrors,
      firebaseMessage
    } = this.state;
    return (
      <div className="custom-body">
        <div className="container">
          <div className="card card-container">
            <img id="profile-img" className="profile-img-card" src={Img} alt="userDemoImage" />
            {firebaseMessage &&  
                <p className={ firebaseMessage.type==='SUCCESS' ? "text-success" : "text-error"} >{firebaseMessage.text || ''}</p>
            }
            <div className="form-signin mb-1">
              <Input
                type="email"
                className="form-control"
                placeholder="Email address"
                value={email}
                error={formErrors['email']}
                onChange={e => this.handleInputChange('email', e)}
              />

              <button className="btn btn-lg btn-primary btn-block btn-signin" type="submit" onClick={this.handleSubmit}>
                Send a reset password mail
              </button>
            </div>
            <Link className="forgot-password" to="/login">
              Sign In
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default ForgotPassword;
