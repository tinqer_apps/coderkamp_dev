export function allInputsAreFilled(form){
    const inputs = Object.keys(form);
    const formErrors = {};
    let isValid = true;
    inputs.forEach(input=>{
        if(!form[input]){
            formErrors[input] = `${input.toUpperCase()} is required.`;
            isValid = false;
        }
    });
    return isValid ? null : formErrors;
}

export function validateEmail(email) {
    const re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    return re.test(email);
}