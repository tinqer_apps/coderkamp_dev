import React from 'react';
import { Link } from 'react-router-dom';
import { logout } from '../../services/firebase';

const Navbar = ({ displayName, loggedIn }) => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light shadow p-3">
        <Link to="/" className="navbar-brand">
          CK
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <Link className="nav-link" to="/">
                Home
              </Link>
            </li>
            {loggedIn && (
              <li className="nav-item">
                <Link to="/dashboard" className="nav-link">
                  Dashboard
                </Link>
              </li>
            )}
            <li className="nav-item">
              <Link to="/share" className="nav-link">
                Editor
              </Link>
            </li>
          </ul>
          {loggedIn ? (
            <ul className="navbar-nav" style={{ width: '100%', justifyContent: 'flex-end' }}>
              <li className="nav-item"> {displayName} </li>
              <li className="nav-item ml-2 pointer" onClick={logout}>
                Logout
              </li>
            </ul>
          ) : (
            <ul className="navbar-nav" style={{ width: '100%', justifyContent: 'flex-end' }}>
              <li className="nav-item">
                <Link className="nav-link" to="/login">
                  Login
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/register" className="nav-link">
                  Register
                </Link>
              </li>
            </ul>
          )}
        </div>
      </nav>
    </div>
  );
};

export default Navbar;