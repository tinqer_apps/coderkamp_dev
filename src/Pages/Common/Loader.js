import React from 'react';

const Loader = () => {
  return (
    <div className="d-flex justify-content-center">
      <div className="d-flex  flex-column justify-content-center">Loading...</div>
    </div>
  );
};

export default Loader;
