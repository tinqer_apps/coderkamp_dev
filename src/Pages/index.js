import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';

import Navbar from './Common/Navbar';
import Loader from './Common/Loader';
import Editor from './Editor';
import Home from './Home';
import Login from './Auth/Login';
import Register from './Auth/Register';
import ForgotPassword from './Auth/ForgotPassword';
import Dashboard from './Dashboard';

const Routing = props => {
  const { auth } = props;
  return auth.isLoaded ? <RoutesWithProfile uid={auth.uid} /> : <Loader />;
};

const Routes = ({ myProfile = {}, auth, uid }) => {
  const loggedIn = Boolean(uid);
  const displayName = (auth || {}).displayName || (myProfile || {}).displayName || '';
  return (
    <Router>
      <Fragment>
        <Navbar loggedIn={loggedIn} displayName={displayName} />
        <Route path="/" exact component={Home} />
        <Route exact path="/share" component={Editor} />
        <Route exact path="/share/:sessionID" component={Editor} />
        <AuthRoute exact path="/login" component={Login} loggedIn={loggedIn} />
        <AuthRoute exact path="/register" component={Register} loggedIn={loggedIn} />
        <AuthRoute exact path="/forgotPassword" component={ForgotPassword} loggedIn={loggedIn} />
        <PrivateRoute exact path="/dashboard" component={Dashboard} loggedIn={loggedIn} />
      </Fragment>
    </Router>
  );
};

const RoutesWithProfile = compose(
  firebaseConnect(({ uid }) => (uid ? [{ path: `users/${uid}`, storeAs: 'myProfile' }] : [])),
  connect((state, props) => ({
    myProfile: state.firebase.data.myProfile,
    auth: state.firebase.auth,
  }))
)(Routes);

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (Boolean(rest.loggedIn) === true ? <Component {...props} /> : <Redirect to="/login" />)} />
);

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (Boolean(rest.loggedIn) === false ? <Component {...props} /> : <Redirect to="/dashboard" />)}
  />
);

const enhance = connect(state => ({
  auth: state.firebase.auth,
}));

export default enhance(Routing);
