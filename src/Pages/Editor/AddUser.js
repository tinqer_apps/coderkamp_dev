import React from 'react';

class AddUser extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          userName: props.displayName || '',
          errorMsg: '',
          enableChat : false
        };
      }
      handleChange = event => {
        this.setState({ userName: event.target.value });
      };
    
      handleSubmit = () => {
        const { userName, enableChat } = this.state;
        if (userName.length === 0) {
          this.setState({ errorMsg: 'Username is required.' });
        } else if (userName.length < 3) {
          this.setState({ errorMsg: 'Minimum 3 characters required.' });
        } else {
          this.props.onSubmit(userName, enableChat);
          this.setState({ userName: '', errorMsg: '' });
        }
      };

      handleCheck =()=> {
        this.setState({
          enableChat: !this.state.enableChat
        })
      }
    componentWillReceiveProps(nextProps){
      this.setState({ userName : nextProps.displayName });
    }
    render(){
        const { userName, enableChat, errorMsg } = this.state;
        const { isNew } = this.props;   
        return (
            <div className="ck-add-user">
              <div className="ck-user-container">
              <div className="ck-user-box">
                <div className="">
                  <div className="form-group">
                    <input type="text" className="form-control"  value={userName} onChange={this.handleChange} placeholder="Enter Your Username" autoFocus />
                  </div>
                  { isNew && 
                    <div className="form-check mb-3 text-left">
                      <input type="checkbox" className="form-check-input " id="enableChat" checked={enableChat} onChange={this.handleCheck} />
                      <label className="form-check-label d-block" style={{"color":"black"}} htmlFor="enableChat">Enable Chatting</label>
                    </div>
                  }
                  <button className="btn btn-block btn-primary"  onClick={this.handleSubmit}>{ isNew ? 'Create' : 'Join'} Session</button>
                </div>
                <br/> 
                {errorMsg && <div className="alert alert-danger text-center">{errorMsg}</div>}
                </div>
              </div>
            </div>
          );
    }
}

export default AddUser;
