import React, { Fragment } from 'react';
import languages from './languages';
import themes from './Themes';

function Header({ selectedMode, selectedTheme, changeTheme, changeMode, show }) {
  return (
    <Fragment>
      <div className="ck-logo">
        <h3 className="navbar-brand ml-2">Editor</h3>
      </div>
      {show &&
        <div className="">
          <select type="select" name="theme" value={selectedTheme} onChange={e => changeTheme(e.target.value)}>
            {themes.map(l => (
              <option key={l.value} value={l.value}>
                {l.text}
              </option>
            ))}
          </select>
        </div>
      }
      {show &&
        <div className="">
          <select type="select" name="language" value={selectedMode} onChange={e => changeMode(e.target.value)}>
            {languages.map(l => (
              <option key={l.value} value={l.value}>
                {l.text}
              </option>
            ))}
          </select>
          {/* <a href={window.location.origin} className="new-icon" target="__blank"><span role="img" aria-label="new_tab">🆕</span></a> */}
        </div>
      }
    </Fragment>
  );
}

export default Header;
