import React, { Fragment } from 'react';

const Sidebar = ({ users, userId, openSidebar, toggleSidebar }) => {
  return (
    <Fragment>
    <div className="ck-user-list ck-toggle" style={{height : '36px'}}>
    <div className="ck-user-circle icon" onClick={toggleSidebar} ><span role="img" aria-label="arrow"> {openSidebar ? <i className="fas fa-angle-double-left"></i> : <i className="fas fa-angle-double-right"></i>}</span> </div>
    {openSidebar && <div className="ck-head-title">
      <span>&nbsp;User Panel</span>
    </div>}
  </div>
    <ul className="ck-user-group ck-sidebar" id="ck-scrollbar">
      {users.map(user => {
        return (
          <li key={user.userId} className="ck-user-list">
            <div className={user.color!=='#fff' ? 'ck-dot ck-online-user' : "ck-dot ck-offline-user"} />
            <div className={'ck-user-circle'} style={{ backgroundColor: user.color }}>
              {user.userName[0].toUpperCase()}
            </div>
            <div className="ck-username">
              <span className={userId === user.userId ? 'ck-me' : ''}>
                {' '}
                &nbsp;{user.userName} {userId === user.userId ? ` | Me` : ''}
              </span>
            </div>
          </li>
        );
      })}
    </ul>
    </Fragment>
  );
};

export default Sidebar;
