import React from 'react';
import { withRouter } from 'react-router-dom'
import firebase from 'firebase/app';

import Header from './Header';
import Editor from './Editor';
import UserBar from './Sidebar';
import Chat from './Chat';
import AddUser from './AddUser';

import { editorConfig } from '../../config/app.config';
// import Footer from './Footer';

import {
  firebaseSessionPath,
  getFirebaseRef,
  updateEditorMode,
  offListening,
  addUserName,
  addSession,
  // confirmBeforeLeave,
  getUserData,
  saveUserData,
  updateEditorTheme,
  // onConnect,
  // onDisconnect
} from '../../services/firebase';

import './index.css';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading : true,
      invalidSessionKey : false,
      selectedMode: editorConfig.language,
      selectedTheme: editorConfig.theme,
      firebaseSessionKey: "",
      firebaseRef : null,
      userId: null,
      userName: '',
      users: [],
      openSidebar: false,
      messages: [],
      openChatbar: false,
      enableChat : false,
      sessionDetails : {} 
    };
  }

  initApp = (refKey, userId, userName = this.state.userName) => {
    const isNewSession = !Boolean(this.props.sessionID);
    // this.changeMode(this.state.selectedMode);
    this.watchEditorModeChangesIntoFirebase(refKey);
    this.watchEditorUserChnagesIntoFirebase(refKey);
    if (userId) {
      // onConnect(refKey, userId, userName);
      // onDisconnect(refKey, userId);
      // confirmBeforeLeave(refKey, userId);
      this.toggleChatbar();
    }
    if(isNewSession){
      // set session Id in URL
      window.location = window.location.origin + '/share/' + refKey;
    }
  };

  validateRefId = () =>{
    const { sessionID } = this.props;
    if(!sessionID){
      this.setState({
        invalidSessionKey : false,
        loading : false
      })
      return;
    }
    const refPath = getFirebaseRef(sessionID);
    refPath
    .once('value', snapshot => {
      this.setState({ loading : false });
      const data = snapshot.val();
        if(data){
          this.props.addSessionToUserAccount(refPath.key, (data.session || {}).ownerId)
          this.setState({
            firebaseSessionKey: refPath.key,
            firebaseRef : refPath,
            enableChat : (data.session || {}).enableChat,
            sessionDetails : data.session
          });
          this.loadUser(refPath.key);
        }else{
          this.setState({
            invalidSessionKey : true
          })
        }
    });
  }


  changeMode = mode => {
    this.setState({ selectedMode: mode });
    updateEditorMode(this.state.firebaseSessionKey, mode);
  };

  changeTheme = theme => {
    this.setState({ selectedTheme: theme });
    updateEditorTheme(this.state.firebaseSessionKey, theme);
  };

  handleUserNameSubmit = (username, enableChat) => {
    const firebaseSession = getFirebaseRef(this.props.sessionID);
    const isNewSession = !Boolean(this.props.sessionID);
    const userId = Math.floor(Math.random() * 9999999999).toString();
    const ownerId = this.props.userFirebaseId || 'anonymous';
    isNewSession && this.setState({ loading : true });
    saveUserData(firebaseSession.key, userId, username, enableChat);
    const promise = isNewSession 
          ? addSession(firebaseSession.key, ownerId, userId, username, enableChat)
          : addUserName(firebaseSession.key, userId, username);
    promise     
    .then((d)=>{
      if(isNewSession){
        this.props.history.push(`/share/${firebaseSession.key}`);
      }else{
        this.setState({
          userName: username,
          userId,
          firebaseRef : firebaseSession,
          firebaseSessionKey : firebaseSession.key
        });
         this.initApp(firebaseSession.key, userId, username);
      }
    })
  };

  toggleSidebar = () => {
    this.setState({ openSidebar: !this.state.openSidebar });
  };

  toggleChatbar = () => {
    this.state.enableChat && this.setState({ openChatbar: !this.state.openChatbar });
  }

  watchEditorModeChangesIntoFirebase = (firebaseSessionKey) => {
    firebase
      .database()
      .ref(`${firebaseSessionPath}/${firebaseSessionKey}/mode`)
      .on('value', snapshot => {
        const updateMode = snapshot.val();
        updateMode && this.setState({ selectedMode: updateMode });
      });

    // Watch Theme change in firebase. 
    firebase
      .database()
      .ref(`${firebaseSessionPath}/${firebaseSessionKey}/theme`)
      .on('value', snapshot => {
        const updateTheme = snapshot.val();
        updateTheme && this.setState({ selectedTheme: updateTheme });
      });
  };

  watchEditorUserChnagesIntoFirebase = (firebaseSessionKey) => {
    firebase
      .database()
      .ref(`${firebaseSessionPath}/${firebaseSessionKey}/users`)
      .on('value', snapshot => {
        const updatedUsers = snapshot.val();
        updatedUsers &&
          this.setState({
            users: Object.keys(updatedUsers).map((key, index) => ({
              userId: key,
              color: updatedUsers[key].color || '#fff',
              userName: updatedUsers[key].userName || `User ${index + 1}`,
              active: updatedUsers[key].active
            })).sort(a => !(a.active || a.color !== '#fff')).filter(u => u.userId)
          });
      });
  };

  loadUser(firebaseSessionKey){
    const data = firebaseSessionKey ? getUserData(this.state.firebaseSessionKey) : "";
    if (data) {
      this.setState({
        firebaseSessionKey,
        userName: data.userName,
        userId: data.userId,
        enableChat : Boolean(data.enableChat)
      });
      this.initApp(this.state.firebaseSessionKey, data.userId, data.userName);
    }
  }
  
  getMainClass = () => {
    const { openChatbar, openSidebar } = this.state;
    if (!openChatbar && !openSidebar) {
      return 'ck-main ck-hide-both'
    }
    if (!openChatbar) {
      return 'ck-main ck-hide-chat'
    }
    if (!openSidebar) {
      return 'ck-main ck-hide-user-list'
    }
    return 'ck-main';
  }

  componentDidMount() {
    this.validateRefId();
  }

  componentWillUnmount() {
    const { firebaseSessionKey } = this.state;
    offListening(firebaseSessionKey, 'mode');
    offListening(firebaseSessionKey, 'users');
    offListening(firebaseSessionKey, 'messages');
    offListening(firebaseSessionKey, 'theme');
  }
  render() {
    const { sessionID, displayName,  width = '500px', height = '400px' } = this.props;
    const {
      loading,
      invalidSessionKey,
      selectedMode,
      selectedTheme,
      firebaseRef,
      firebaseSessionKey,
      userName,
      userId,
      users,
      openSidebar,
      openChatbar,
      enableChat
      
    } = this.state;
    const showEditor = userId;
    if(loading){
      return (
        <div className="m-4 text-center">
             <h4>Loading...</h4>
        </div>
      )
    }
    if(invalidSessionKey){
      return (
        <div className="m-4 text-center">
             <h4>Invalid Session Key</h4>         
        </div>
      ) 
    }
    return (
      <div style={{ width: width, height: height }}>
        <div className={this.getMainClass()}>
          <div className="ck-toolbar py-1">
            <Header selectedMode={selectedMode} selectedTheme={selectedTheme}
              changeTheme={this.changeTheme}
              changeMode={this.changeMode} show={showEditor} />
          </div>
          <div className="ck-directory" style={{ height: `calc(${height} - 70px)` }}>
          {userName && userId && <UserBar users={users} userId={userId} openSidebar={openSidebar} toggleSidebar={this.toggleSidebar} /> }
          </div>
          <div className="ck-editor" style={{marginRight: '10px'}}>
            {showEditor ? (
              <Editor mode={selectedMode} selectedTheme={selectedTheme} userId={userId} firebaseRef={firebaseRef} />
            ) : (
                <AddUser isNew={!sessionID} displayName={displayName}  onSubmit={this.handleUserNameSubmit} />
              )}
          </div>
          <div className={openChatbar ? "ck-chat-box" : "hide"} style={{ height: `calc(${height} - 70px)` }}>
            {userName && userId && <Chat userName={userName} openChatbar={openChatbar} toggleChatbar={this.toggleChatbar} firebaseSessionKey={firebaseSessionKey} userId={userId} />}
          </div>
          <div className="ck-footer " />
        </div>
        {enableChat && showEditor && !openChatbar && <div className="ck-floating-btn" onClick={this.toggleChatbar}><span role="img" aria-label="left_arrow">‹</span></div>}
      </div>
    );
  }
}

export default withRouter(Main);