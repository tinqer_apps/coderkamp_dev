import React, { Fragment } from 'react';
import firebase from 'firebase/app';
import { firebaseSessionPath, sendMessage } from '../../services/firebase';

class Chat extends React.Component {
  constructor() {
    super();
    this.state = {
      message: '',
      messages: []
    };
    this.myRef = React.createRef();
  }
  watchMessagesIntoFirebase = () => {
    firebase
      .database()
      .ref(`${firebaseSessionPath}/${this.props.firebaseSessionKey}/messages`)
      .on('value', snapshot => {
        const data = snapshot.val();
        if (data) {
          const msgs = Object.keys(data).map(k => ({ ...data[k], id: k, timestamp: formatDate(data[k].timestamp) }));
          this.setState({
            messages: msgs
          });
          this.scrollToBottom();
        }
      });
  };
  scrollToBottom = () => {
    this.myRef.current.scrollTop = this.myRef.current.scrollHeight;
  };
  handleSubmit = () => {
    if (this.state.message.length > 0) {
      sendMessage(this.props.firebaseSessionKey, this.props.userName, this.props.userId, this.state.message);
      this.setState({ message: '' });
    }
  };
  handleChange = event => {
    this.setState({
      message: event.target.value
    });
  };
  componentDidMount() {
    this.watchMessagesIntoFirebase();
  }
  componentDidUpdate() {
    this.scrollToBottom();
  }
  render() {
    const { openChatbar, toggleChatbar } = this.props;
    const { message, messages } = this.state;
    return (
      <Fragment>
        <div className="ck-user-list ck-toggle" style={{ height: '45px' }}>
          <div className="ck-user-circle icon" onClick={toggleChatbar}>
            <span role="img" aria-label="arrow">
              {' '}
              {!openChatbar ? '‹' : '›'}
            </span>{' '}
          </div>
          <div className="ck-head-title">
            <span>&nbsp;Chat</span>
          </div>
        </div>
        <div className="ck-abc" id="ck-scrollbar" ref={this.myRef}>
          {messages.map(msg => (
            <div
              className={
                msg.userId === this.props.userId ? 'ck-talk-bubble ck-msg-right ck-right' : 'ck-talk-bubble ck-msg-left'
              }
              key={msg.id}
            >
              <p className="ck-sub">{msg.userName}</p>
              <div className="ck-talktext">
                <p dangerouslySetInnerHTML={{ __html: msg.message.replace(/\n/g, '<br/>') }} />
              </div>
            </div>
          ))}
        </div>
        <div className="ck-msg-input">
          <textarea value={message} onChange={this.handleChange} />
          <button onClick={this.handleSubmit}>
            <span role="img" aria-label="send">
              ▶
            </span>
          </button>
        </div>
      </Fragment>
    );
  }
}

export default Chat;

function formatDate(d) {
  const date = new Date(d);
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const day = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  return `${hours}:${minutes} ${ampm} ${day} ${monthNames[monthIndex]} ${year}`;
}
