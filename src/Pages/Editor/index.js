import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firebaseSet } from '../../services/firebase'
import Main from './Main';

class Editor extends Component {
  
  addSessionToUserAccount=(sessionKey, ownerId)=>{
    const { uid } = this.props.auth || {};
    if(uid){
      const ref = uid===ownerId ? 'usersCreatedSessions' : 'usersJoinedSessions';
      firebaseSet(`${ref}/${uid}/${sessionKey}`,{
        timestamp : Date.now()
      })
    }
  }

  render() {
    const {
      match: {
        params: { sessionID }
      },
      auth,
      myProfile
    } = this.props;
    const userFirebaseId = (auth || {}).uid || null;
    const displayName = (auth || {}).displayName || (myProfile || {}).displayName || '';
    return (
      <div id="main-editor">
        <Main
          width={`100%`}
          height={`${window.screen.availHeight - 140}px`}
          sessionID={sessionID}
          userFirebaseId={userFirebaseId}
          displayName={displayName}
          addSessionToUserAccount={this.addSessionToUserAccount}
        />
      </div>
    );
  }
}

const enhance = connect(state => ({
  auth: state.firebase.auth,
  myProfile: state.firebase.data.myProfile,
  firebase : state.firebase
}));

export default enhance(Editor);
