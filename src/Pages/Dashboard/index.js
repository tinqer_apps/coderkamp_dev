import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';

const Dashboard = (props) => {
  const { usersCreatedSessions, usersJoinedSessions } = props;
  const url = window.location.origin;
  return (
    <div className="container px-4 mt-4">
      <h2>Dashboard</h2>
      <div className="">
        <div className="my-4">
          <h5>Created Sessions</h5>
          <ul className="list-group">
          {
            usersCreatedSessions.map((session, index)=>(
              <li className="list-group-item" key={session.key}> {index + 1}. <a href={`/share/${session.key}`} target="__blank"> {url}/share/{ session.key } </a></li>
            ))
          }
          { usersCreatedSessions.length===0 && <li className="list-group-item text-center">No session exist yet!</li>}
          </ul>
        </div>
        <div className="mt-4">
          <h5>Joined Sessions</h5>
          <ul className="list-group">
          {
            usersJoinedSessions.map((session, index)=>(
              <li className="list-group-item" key={session.key}> {index + 1}. <a href={`/share/${session.key}`} target="__blank"> {url}/share/{ session.key } </a></li>
            ))
          }
          { usersJoinedSessions.length===0 && <li className="list-group-item text-center">No session exist yet!</li>}

          </ul>

        </div>
        
      </div>
    </div>
  );
};

export default compose(
  connect((state, props) => ({
    uid: (state.firebase.auth || {}).uid
  })),
  firebaseConnect(({ uid }) =>
    uid ? [{ path: `usersCreatedSessions/${uid}` }, { path: `usersJoinedSessions/${uid}` }] : []
  ),
  connect((state, props) => ({
    usersCreatedSessions: (state.firebase.ordered.usersCreatedSessions || {})[props.uid] || [],
    usersJoinedSessions: (state.firebase.ordered.usersJoinedSessions || {})[props.uid] || []
  }))
)(Dashboard);
