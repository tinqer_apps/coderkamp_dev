import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div className="container text-center  px-4">
      <div className="p-4 mt-4">
        <Link to="/share">
          <button className="btn  btn-light">Open Editor</button>
        </Link>
      </div>
    </div>
  );
};

export default Home;
